package task4;

public interface Logger {
    void logInfo(String message);
    void logWarning(String message);
    void logError(String message);
}