package task4;

class FileLogger implements Logger {
    @Override
    public void logInfo(String message) {
        writeLogToFile("INFO " + message);
    }

    @Override
    public void logWarning(String message) {
        writeLogToFile("WARNING " + message);
    }

    @Override
    public void logError(String message) {
        writeLogToFile("ERROR " + message);
    }

    private void writeLogToFile(String logMessage) {
        System.out.println("Log message: " + logMessage);
    }
}