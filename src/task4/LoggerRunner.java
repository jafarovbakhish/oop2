package task4;

public class LoggerRunner {

    public static void main(String[] args) {
        Logger consoleLogger = new ConsoleLogger();
        consoleLogger.logInfo("Information message");
        consoleLogger.logWarning("Warning message");
        consoleLogger.logError("Error message");

        System.out.println();

        Logger fileLogger = new FileLogger();
        fileLogger.logInfo("Information message");
        fileLogger.logWarning("Warning message");
        fileLogger.logError("Error message");
    }
}
