package task3;

public abstract class Media {
    private String name;
    private int length;

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public abstract void play() ;
}