package task3;

public class MediaPlayRunner {

    public static void main(String[] args) {
        Music music = new Music();
        music.setName("Believer");
        music.setLength(5);
        music.play();

        Movie movie = new Movie();
        movie.setName("Titanic");
        movie.setLength(176);
        movie.play();
    }
}