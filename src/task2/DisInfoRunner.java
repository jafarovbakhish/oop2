package task2;

public class DisInfoRunner{
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Baxish");
        student.setAge(20);
        student.setStudentID(00001);
        student.displayInfo();

        System.out.println();

        Teacher teacher = new Teacher();
        teacher.setName("Nur");
        teacher.setAge(27);
        teacher.setSubject("Programming");
        teacher.displayInfo();
    }
}
