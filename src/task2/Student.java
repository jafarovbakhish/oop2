package task2;

public class Student extends Person {
    private int studentID;

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    @Override
    public void displayInfo() {
        System.out.println(getName());
        System.out.println( getAge());
        System.out.println( getStudentID());
    }
}
