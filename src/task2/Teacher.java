package task2;

public class Teacher extends Person {
    private String subject;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public void displayInfo() {
        System.out.println( getName());
        System.out.println( getAge());
        System.out.println( getSubject());
    }
}
